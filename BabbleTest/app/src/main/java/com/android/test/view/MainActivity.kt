package com.android.test.view

import android.os.Bundle
import com.android.test.R


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentUtils.replaceFragment(R.id.container, GameFragment())
    }

    /*
    * back press method for clearing stacks and close application
    */
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1)
            finish()
        else
            supportFragmentManager.popBackStack()
    }

}
