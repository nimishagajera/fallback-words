package com.android.test.view


import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation

import com.android.test.R
import com.android.test.model.ResultModel
import com.android.test.util.getRandomColor
import kotlinx.android.synthetic.main.fragment_game.*
import org.json.JSONArray
import java.util.*

class GameFragment : BaseFragment() {

    private var listEnglishWords: LinkedList<String> = LinkedList()
    private var listSpanishWords: LinkedList<String> = LinkedList()
    private var count = 0
    private var isAnswered = false
    private var correct = 0
    private var wrong = 0
    private var noAnswer = 0

    companion object {
        var resultModel: ResultModel = ResultModel()
    }

    /*
     * onViewCreated method for view
     * @param inflater: LayoutInflater
     * @param container: ViewGroup
     * @param savedInstanceState: Bundle
     * @return View
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    /*
    * on resume method for clearing old results
    */
    override fun onResume() {
        super.onResume()
        resultModel = ResultModel()
    }
    /*
    * onViewCreated method used for initialization
    * @param view: View
    * @param savedInstanceState: Bundle
    */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val wordStr = resources.openRawResource(R.raw.words_v2)
                .bufferedReader().use { it.readText() }

        val wordArray = JSONArray(wordStr)
        for (i in 0 until wordArray.length()) {
            val wordObj = wordArray.getJSONObject(i)
            val stringEnglish = wordObj.getString("text_eng")
            val stringSpanish = wordObj.getString("text_spa")

            listEnglishWords.add(stringEnglish)
            listSpanishWords.add(stringSpanish)
        }

        setTextAndColor()
        animation(false)

        btn_game_over.setOnClickListener {
            fragmentUtils.addFragment(R.id.container, ResultFragment())
        }

        btn_correct.setOnClickListener {
            if (matchAnswer())
            correct += 1

            resultModel.correctAnswer = correct.toString()
            isAnswered = true
            animation(true)
        }

        btn_wrong.setOnClickListener {
            if (!matchAnswer())
                wrong += 1
            isAnswered = true
            resultModel.wrongAnswer = wrong.toString()
            animation(true)
        }
    }

    /*
    * matchAnswer used for checking answer is correct or not on click on button
    */
    private fun matchAnswer(): Boolean {
        val english = txt_english.text.toString()
        val spanish = txt_spanish.text.toString()
        for (i in 0 until listEnglishWords.size) {
            for (j in i until listSpanishWords.size) {
                if (listEnglishWords[i] == english) {
                    val spanishWord = listSpanishWords[j]
                    return spanish == spanishWord
                }
            }
        }
        return false
    }

    /*
    * Animation called with condition if button pressed, it will create new animation with updated text
    */
    private fun animation(isButtonClicked: Boolean) {
        var mAnimation = translateAnimation()

        if (isButtonClicked){
            mAnimation.cancel()
        }

        mAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {

            }
            override fun onAnimationEnd(animation: Animation?) {
                if (!isAnswered) {
                    noAnswer += 1
                    resultModel.noAnswer = noAnswer.toString()
                }

                if (isAnswered)
                    isAnswered = false

                count++
                mAnimation = translateAnimation()
                mAnimation.setAnimationListener(this)
                setTextAndColor()
            }
        })
    }

    /*
    * Text updates with random text color on each animation end
    */
    private fun setTextAndColor() {
        if (count%2 == 0) {
            txt_english.text = listEnglishWords[count]
            txt_spanish.text = selectRandomWord(listSpanishWords)
        } else {
            txt_english.text = listEnglishWords[count]
            txt_spanish.text = listSpanishWords[count]
        }

        txt_english.setTextColor(getRandomColor(mContext))
        txt_spanish.setTextColor(getRandomColor(mContext))
    }

    private fun selectRandomWord(list:LinkedList<String>):String {
        return list.get(Random().nextInt(list.size))
    }

    /*
    * Animation used for text to fallback from to bottom in 10 secs
    * Time and count can be change by coder
    */
    private fun translateAnimation(): TranslateAnimation {
        val mAnimation = TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f)
        mAnimation.duration = 10000
        mAnimation.interpolator = LinearInterpolator()
        txt_spanish.animation = mAnimation

        if (count > 15){
            btn_game_over.visibility = View.VISIBLE
            txt_english.visibility = View.GONE
            txt_spanish.visibility = View.GONE
            txt_spanish.clearAnimation()
        }
        return mAnimation
    }

}
