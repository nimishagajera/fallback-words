package com.android.test.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.android.test.util.FragmentUtils

/*
* Simple Base Fragment can be used for sub class and comman method with protected access modifiers
*/
open class BaseFragment: Fragment() {

    protected lateinit var fragmentUtils: FragmentUtils

    protected lateinit var mContext:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentUtils = FragmentUtils(fragmentManager)
    }
}