package com.android.test.model

/*
* Model class for result
*/
class ResultModel {

    var correctAnswer: String? = "0"
    var wrongAnswer: String? = "0"
    var noAnswer: String? = "0"
    override fun toString(): String {
        return "ResultModel(correctAnswer=$correctAnswer, wrongAnswer=$wrongAnswer, noAnswer=$noAnswer)"
    }
}