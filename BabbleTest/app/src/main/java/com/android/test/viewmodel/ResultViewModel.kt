package com.android.test.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.android.test.BR
import com.android.test.model.ResultModel

class ResultViewModel(private val resultModel: ResultModel): BaseObservable() {

    var correct:String?
    @Bindable
    get() = resultModel.correctAnswer
    set(value) {
        resultModel.correctAnswer = value
        notifyPropertyChanged(BR.correct)
    }

    var wrong:String?
    @Bindable
    get() = resultModel.wrongAnswer
    set(value) {
        resultModel.wrongAnswer = value
        notifyPropertyChanged(BR.wrong)
    }

    var noAnswer:String?
    @Bindable
    get() = resultModel.noAnswer
    set(value) {
        resultModel.noAnswer = value
        notifyPropertyChanged(BR.noAnswer)
    }
}