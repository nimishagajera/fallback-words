package com.android.test.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.android.test.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    /*
    * onViewCreated method for view
    * @param inflater: LayoutInflater
    * @param container: ViewGroup
    * @param savedInstanceState: Bundle
    * @return View
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        btn_play.setOnClickListener {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }
    }
}
