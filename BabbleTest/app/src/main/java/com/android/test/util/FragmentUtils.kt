package com.android.test.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class FragmentUtils(private val fragmentManager: FragmentManager?) {

    /*
    * Code for replacing fragment without BackStack
    */
    fun replaceFragment(container: Int, fragment: Fragment) {
        fragmentManager
                ?.beginTransaction()
                ?.replace(container, fragment)
                ?.addToBackStack(null)
                ?.commit()
    }

    /*
    * Code for adding fragment without BackStack
    */
    fun addFragment(container: Int, fragment: Fragment) {
        fragmentManager
                ?.beginTransaction()
                ?.add(container, fragment)
                ?.addToBackStack(null)
                ?.commit()
    }
}
