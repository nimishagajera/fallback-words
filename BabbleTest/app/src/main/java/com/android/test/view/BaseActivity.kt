package com.android.test.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.test.util.FragmentUtils

/*
* Simple Base Activity can be used for sub class and comman method with protected access modifiers
*/
open class BaseActivity: AppCompatActivity() {

    protected lateinit var fragmentUtils: FragmentUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentUtils = FragmentUtils(supportFragmentManager)
    }
}