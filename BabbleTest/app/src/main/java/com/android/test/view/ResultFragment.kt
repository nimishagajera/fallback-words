package com.android.test.view


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.android.test.R
import com.android.test.databinding.FragmentResultBinding
import com.android.test.model.ResultModel
import com.android.test.viewmodel.ResultViewModel

class ResultFragment : BaseFragment() {

    private lateinit var binding: FragmentResultBinding

    /*
    * onViewCreated method for view
    * @param inflater: LayoutInflater
    * @param container: ViewGroup
    * @param savedInstanceState: Bundle
    * @return View
    */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_result,container,false)
        return binding.root
    }

    /*
      * onViewCreated method used for initialization
      * @param view: View
      * @param savedInstanceState: Bundle
      */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.result = ResultViewModel(GameFragment.resultModel)

        binding.btnPlayAgain.setOnClickListener {
            fragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fragmentUtils.replaceFragment(R.id.container, GameFragment())
        }
    }
}
