package com.android.test.util

import android.content.Context
import com.android.test.R
import java.util.*

/*
* random color generator
*/
fun getRandomColor(context: Context): Int {
    val androidColors = context.resources.getIntArray(R.array.colors)
    return androidColors[Random().nextInt(androidColors.size)]
}